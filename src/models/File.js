import Entity from './Entity'

export default class File extends Entity {
    constructor(data) {
        super(data.file_id)
        this.file_name = data.file_name       
        this.description = data.description
        this.extension = data.extension
        this.path = data.path
        this.size = data.size
        this.title = data.title
        this.url = data.url
        this.type = data.type
        this.file_zip_id = data.file_zip_id
    }

    /**
     * @param {*} data
     */
    static create(data) {
        if (!data) {
            throw new Error('File::create: "data" is undefined')
        }
        return new File(data)
    }
    updateFile(data) {
        this.file_name = data.file_name
        this.description = data.description
        this.extension = data.extension
        this.path = data.path
        this.size = data.size
        this.title = data.title
        this.url = data.url
        this.type = data.type
        this.file_zip_id = data.file_zip_id
    }
}
