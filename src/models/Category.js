import Entity from './Entity'

export default class Category extends Entity {
  constructor(data) {
    super(data.category_id);
    this.category_name = data.category_name;
    this.description = data.description;
    this.enableDisable = data.enableDisable == 1 ? true : false;
  }
  /**
   * @param {*} data
   */
  static create(data) {
    if (!data) {
      throw new Error('Category::create: "data" is undefined');
    }
    return new Category(data);
  }
  update(data) {
    this.category_name = data.category_name;
    this.description = data.description;
    this.enableDisable = data.enableDisable == 1 ? true : false;
  }
}
