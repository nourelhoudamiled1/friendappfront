import Entity from './Entity'

export default class Product extends Entity {
    constructor(data) {

        super(data.product_id)
        this.product_name = data.product_name
        this.long_description = data.long_description
        this.short_description = data.short_description
        this.file_original = data.file_original
        this.file_zip = data.file_zip
        this.file_related = data.file_related
        this.category_id = data.category_id
        this.product_id = data.product_id
        this.enableDisable = data.enableDisable == 1 ? true : false
    }
    /**
     * @param {*} data
     */
    static create(data) {
        if (!data) {
            throw new Error('Product::create: "data" is undefined')
        }
        return new Product(data)
    }
    update(data) {
        this.product_name = data.product_name
        this.long_description = data.long_description
        this.short_description = data.short_description
        this.file_original = data.file_original
        this.file_zip = data.file_zip
        this.file_related = data.file_related
        this.category_id = data.category_id
        this.product_id = data.product_id
        this.enableDisable = data.enableDisable == 1 ? true : false

    }
}
