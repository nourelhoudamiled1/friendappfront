import Vuex from "vuex";
import Vue from "vue";

import { createStore } from "vuex-extensions";
import product from "./product";
import category from "./category";
Vue.use(Vuex);

export default createStore(Vuex.Store, {
  state: {
    /**
     * Authentication
     */
    currentUser:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI0IiwianRpIjoiM2VkNDIyZmEwZjk2YWM0ODEyMWE1NzNkNmM3ZTk4OWIwMTExMDIwOTFmNGViNTdiZmNmNGEwZDQyY2Q4MmE3ZWE4NzhjMzFiOWQ0OThkNzgiLCJpYXQiOjE2ODUwMjQ5NjMuOTU2NjA5LCJuYmYiOjE2ODUwMjQ5NjMuOTU2NjE3LCJleHAiOjE3MTY2NDczNjIuMjA3NDEyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.RK2lUIOqykhJSzZtXFtNxTdkVLF9hWFoD3z1McemeqFSnYsnYfdzgzD8IBuGRAMrJBD4a3kxjdgO4uoITbBoR2UtZEfCeC1YAxtsnE5hhdH-__6x8phmRMFqt6WmhbXDB9-27YZZgbr8Gh-fbKEC6tLvVNlHZfNaxdzzQcwoL46NiBdGAleS2Lmz2plqIBomchJv26UIYSvvjhL4Fq2bwyVc9EAW1yRjUNp64RE2EGB2WvxWAhXFn64vpJfkHEhmPifUc_UnKWXJLv2vLWhG57C_TktNSbX9mIVXNi8qdRT5Tmz6mGqOtjNm1LV7JE2gnaO-v9-roAIPWUe2H3ej0q-vORRySANjqPoyBI1qbeVBISvcY6h1mVxRTZF-APuSMQK43l0dp3QFnA1hl9XNxxOokng1ZK0inqXGJX2f0eCtoS8fqCcOUl_49sS9ITVIwzeQF2q41wJoP-PJGpa5vifdevRU7E_xPGexb4zQMugQb9u8rHUCU-d8UIyZuw6SGpg9I1OInyx_39vxhrOvhUAaKcVj-oqJGj6RmhHO_cUBMSCUbFWOeFSRShozT8vwq_v7R-1MdE8ZKhx384HkwA5Zpadt_U3j_IeI1AwgQccjV_uEz3dQ81wYu-pAaRWdC-xRNNcbYDGQSvtL-nMfQis4YN4SOsiKBhInMiYhPWQ",
   
  },
  modules: {
    category,
    product
  },
});
