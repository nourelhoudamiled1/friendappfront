import Axios from "axios";
import Category from "../models/Category";
import domain from "@/environment";
const state = {
  categories: null,
  categoriesLoading: false,
  categoriesError: false,
};
const getters = {
  getCategories: (state) => state.categories,
  getCategoriesLoading: (state) => state.categoriesLoading,
  getCategoryError: (state) => state.categoriesError,
};
const mutations = {
  SET_CATEGORIES_LOADING(state, payload = false) {
    state.categoriesLoading = payload;
  },
  SET_CATEGORIES_ERROR(state, payload = null) {
    state.categoriesError = payload;
  },

  // fetch categorie mutations
  SET_CATEGORIES(state, categories) {
    state.categories = categories;
  },

  // Category mutations
  ADD_CATEGORIES(state, { categorie }) {
    state.categories.push(Category.create(categorie));
  },
};
const actions = {
  resetErrorCategories({ commit }) {
    commit('SET_CATEGORIES_ERROR')
  },
  // fetch Categorys
  async fetchCategories({ commit }) {
    commit("SET_CATEGORIES_LOADING", true);
    commit("SET_CATEGORIES_ERROR");
    try {
      const params = {};
      const response = await Axios.get(domain + "/api/categories", {
        headers: {
          Authorization: `Bearer ${this.state.currentUser}`,
        },
        params: params,
      });
      commit("SET_CATEGORIES", response.data);
      commit("SET_CATEGORIES_LOADING");
    } catch (error) {
      commit("SET_CATEGORIES_LOADING");
      if (
        error.response &&
        error.response.data
      ) {
        commit("SET_CATEGORIES_ERROR", error.response.data);
      } else {
        commit("SET_CATEGORIES_ERROR", ["Une erreur est survenue"]);
      }
      return false;
    }
    return true;
  },
};
export default {
  state,
  getters,
  mutations,
  actions,
};
