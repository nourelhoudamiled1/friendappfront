import Axios from "axios";
import Product from "../models/Product";
import domain from "@/environment";
const state = {
    products: null,
    productsLoading: false,
    productsError: false,
};
const getters = {
    getProducts: (state) => state.products,
    getProductsLoading: (state) => state.productsLoading,
    getProductsError: (state) => state.productsError,
};
const mutations = {
    SET_PRODUCTS_LOADING(state, payload = false) {
        state.productsLoading = payload;
    },
    SET_PRODUCTS_ERROR(state, payload = null) {
        state.productsError = payload;
    },
    // fetch PRODUCT mutations
    SET_PRODUCTS(state, products) {
        state.products = products;
    },
    // Product mutations
    SAVE_PRODUCT(state, product) {
        Product.create(product);
    },

};
const actions = {
    async resetErrorProducts({ commit }) {
        commit('SET_PRODUCTS_ERROR')
    },
    // fetch Products
    async fetchProducts({ commit }) {
        commit("SET_PRODUCTS_LOADING", true);
        commit("SET_PRODUCTS_ERROR");
        try {
            const params = {};
            const response = await Axios.get(domain + "/api/products", {
                headers: {
                    Authorization: `Bearer ${this.state.currentUser}`,
                },
                params: params,
            });
            commit("SET_PRODUCTS", response.data);
            commit("SET_PRODUCTS_LOADING");
        } catch (error) {
            commit("SET_PRODUCTS_LOADING");
            if (
                error.response &&
                error.response.data &&
                error.response.data.error &&
                error.response.data.error.messages
            ) {
                commit("SET_PRODUCTS_ERROR", error.response.data.error.messages);
            } else {
                commit("SET_PRODUCTS_ERROR", ["Une erreur est survenue"]);
            }
            return false;
        }
        return true;
    },
    async saveProduct({ commit }, payload) {
        commit('SET_PRODUCTS_LOADING', true)
        commit('SET_PRODUCTS_ERROR')
        try {
            const response = await Axios.post(
                domain + '/api/saveProduct',
                payload,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        Authorization: `Bearer ${this.state.currentUser}`
                    }
                }
            )
            commit('SAVE_PRODUCT',
                response.data
            )

            commit('SET_PRODUCTS_LOADING')
        } catch (error) {
            commit('SET_PRODUCTS_LOADING')
            if (
                error.response &&
                error.response.data &&
                error.response.data.error &&
                error.response.data.error.messages
            ) {
                commit('SET_PRODUCTS_ERROR', error.response.data.error.messages)
            } else {
                commit('SET_PRODUCTS_ERROR', ['Une erreur est survenue'])
            }
            return false
        }
        return true
    },
};
export default {
    state,
    getters,
    mutations,
    actions,
};
