import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import custom from "@/assets/customStyle.scss";

// Libraries components
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

// import "./imports/font-awesome";
Vue.config.productionTip = false;
Vue.use(custom);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
