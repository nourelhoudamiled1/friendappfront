import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);
const routes = [
  {
    path: "/",
    name: "Home",
    component: () =>
      import(/* webpackChunkName: "Admin" */ "@/views/HomePage.vue"),
    beforEnter(to, from, next) {
      next();
    },
  },
  {
    path: "/contact",
    name: "Contact",
    component: () =>
      import(/* webpackChunkName: "Admin" */ "@/views/Auth/contact.vue"),
    beforEnter(to, from, next) {
      next();
    },
  },
  {
    path: "/admin",
    name: "Admin",
    component: () =>
      import(/* webpackChunkName: "Admin" */ "@/views/Admin/index.vue"),
    beforEnter(to, from, next) {
      next();
    },
    children: [
      {
        path: "product/list",
        name: "list-product",
        component: () =>
          import(
            /* webpackChunkName: "list-product" */ "@/views/Admin/Product/index.vue"
          ),
        beforEnter(to, from, next) {
          next();
        },
      },
        {
          path: "product/add",
        name: "add-product",
        component: () =>
          import(
            /* webpackChunkName: "add-product" */ "@/views/Admin/Product/productForm.vue"
          ),
        beforEnter(to, from, next) {
          next();
        },
      },
      {
        path: "category",
        name: "Category",
        component: () =>
          import(
            /* webpackChunkName: "Category" */ "@/views/Admin/Category/index.vue"
          ),
      },
    ],
  },
];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
export default router;
